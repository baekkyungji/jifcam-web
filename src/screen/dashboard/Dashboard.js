import React, {Component} from "react";
import Post from "../../components/post/Post";
import PostData from "../../helpers/data/PostData";
import Link from "../../components/link/Link";
import {Helmet} from "react-helmet";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user:''
    }
  }

  componentDidMount() {

  }

  render() {
    const postList = PostData.DATA;
    console.log(postList);
    return (
      <div>
        <Helmet>
          <title>Jifcast</title>
          <meta name="description" content="Dashboard" />
          <meta name="keywords" content="Dashboard" />
        </Helmet>
        <h1>Dashboard</h1>
        {
          postList.map((item,index) => (
            <Post key={index} item={item}/>
          ))
        }
        <Link/>
      </div>
    )
  }
}

export default Dashboard;
