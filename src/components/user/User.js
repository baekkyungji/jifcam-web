import React from "react";

const User = (props) => {
  const {name, avatarUrl} = props.item;

  return (
    <div>
      <div>
        <h1>{name}</h1>
        <img width={300} height={200} src={avatarUrl} alt={'avatar'}/>
      </div>
    </div>
  )
};

export default User;
