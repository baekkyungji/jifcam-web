import React from "react";

const Link = () => {
  return (
    <div>
      <a href="https://www.facebook.com/">Facebook</a><br/>
      <a href="https://www.instagram.com/">Instagram</a><br/>
      <a href="https://www.twitter.com/">Twitter</a>
    </div>
  )
};

export default Link;
