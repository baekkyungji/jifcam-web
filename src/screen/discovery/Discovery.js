import React, {Component} from "react";
import UserData from "../../helpers/data/UserData";
import User from "../../components/user/User";
import Link from "../../components/link/Link";
import {Helmet} from "react-helmet";

class Discovery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user:''
    }
  }

  componentDidMount() {

  }

  componentWillMount() {

  }

  render() {
    const userList = UserData.DATA;
    return (
      <div>
        <Helmet>
          <title>Jifcast</title>
          <meta name="description" content="Discovery" />
          <meta name="keywords" content="Discovery" />
        </Helmet>
        <h1>Discovery</h1>
        {
          userList.map((item,index) => (
            <User key={index} item={item}/>
          ))
        }
        <Link/>
      </div>
    )
  }
}

export default Discovery;
