import React from "react";

const Directory = (props) => {
  const {title, description} = props.item;
  console.log(props);

  return (
    <div>
      <h1>{title}</h1>
      <h3>{description}</h3>
    </div>
  )
};

export default Directory;
