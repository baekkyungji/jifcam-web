import * as React from 'react';
import {connect} from 'react-redux';
import {loadUsers} from "./actions";

class UsersWithReduxThunk extends React.Component {
  componentDidMount() {
    this.props.loadUsers();
  };

  render() {
    if (this.props.loading) {
      return <div>Loading</div>;
    }
    if (this.props.error) {
      return <div style={{color: 'red'}}>
        ERROR: {this.props.error}
      </div>
    }
  }
}
