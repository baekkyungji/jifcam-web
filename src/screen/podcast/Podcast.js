import React, {Component} from "react";
import './Podcast.css';
import {Col, Container, Row} from "react-bootstrap";
import {Helmet} from "react-helmet";

class Podcast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: ''
    }
  }

  generateIndex = () => {
    let indexAlphabet = [];
    for (let index = 65; index <= 90; index++) {
      let char = String.fromCharCode(index);
      indexAlphabet.push(<p>{char}</p>);
    }
    return indexAlphabet;
  };

  generateNumber = () => {
    let indexNumber = [];
    for (let index = 0; index <= 9; index++) {
      indexNumber.push(<p>{index}</p>);
    }
    return indexNumber;
  };

  generateDirectoryList = () => {
    return (
      <div className="row">
        <div className="column">
          <p className={'list'}>Directories 1</p>
        </div>
        <div className="column">
          <p className={'list'}>Directories 2</p>
        </div>
        <div className="column">
          <p className={'list'}>Directories 3</p>
        </div>
      </div>
    )
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>Jifcast</title>
          <meta name="description" content="Podcast" />
          <meta name="keywords" content="Podcast" />
        </Helmet>
        <h1>Podcasts</h1>
        <div>
          {this.generateIndex()}
          {this.generateNumber()}
        </div>
        <div>
          {this.generateDirectoryList()}
        </div>
      </div>
    )
  }
}

export default Podcast;
