import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import Dashboard from "./screen/dashboard/Dashboard";
import Discovery from "./screen/discovery/Discovery";
import {Helmet} from "react-helmet";
import Podcast from "./screen/podcast/Podcast";

function App() {
  return (
    <div className="App">
      <Helmet>
        <title>Jifcast</title>
        <meta name="description" content=" react helmet" />
        <meta name="keywords" content="react,seo,helmet" />
      </Helmet>
      <Router>
        <div>
          <h1>Jifcam</h1>
          {/*<ul>*/}
            {/*<li><Link to="/">Dashboard</Link></li>*/}
            {/*<li><Link to="/about">Discovery</Link></li>*/}
            {/*<li><Link to="/list-directory">Podcasts</Link></li>*/}
          {/*</ul>*/}
          <hr/>
          <Switch>
            <Route exact path="/"><Podcast/></Route>
            <Route path="/dashboard"><Dashboard/></Route>
            <Route path="/about"><Discovery/></Route>
            <Route path="/list-directory"><Podcast/></Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
